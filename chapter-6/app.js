const express = require("express");
const app = express();
const { userGame, userBiodata, userHistory } = require("./models");
require("dotenv").config();
const path = require("path");
const response = require("./response");
const {
  handleHome,
  handleLogin,
  handleCreate,
  handleEdit,
  handleDelete,
} = require("./handler/handler");

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.set("view engine", "ejs");

const api = require("./router/api");
app.use("/api", api);

app.set("views", path.join(__dirname, "views"));
app.use(express.static("public"));
app.get("/", handleHome);
app.get("/login", handleLogin);

// const handleCreate = async (req, res) => {
//   const { username, password } = req.body;
//   const user = await userGame.create({
//     username,
//     password,
//   });

//   if (!user) {
//     response.fail(res, `Failed to create new user`);
//     return;
//   }

//   const user_Biodata = await userBiodata.create({
//     fullname: "User 6 Fullname ",
//     address: "Bogor",
//     user_id: user.id,
//   });

//   if (!user_Biodata) {
//     response.fail(res, `Failed to create new user biodata`);
//   }
//   response.success(res, `Created new user`);

//   const user_History = await userHistory.create({
//     lastlogin: "Sunday, 20210517",
//     score: "331",
//     user_id: user.id,
//   });

//   if (!user_History) {
//     response.fail(res, `Failed to create new history`);
//     return;
//   }
//   response.success(res, `Created new history`);
// };

// const handleDelete = (req, res) => {
//   const { userID } = req.body;
//   userGame
//     .destroy({
//       where: {
//         id: userID,
//       },
//     })
//     .then(async (success) => {
//       if (success) {
//         // await user.destroy();
//         // Now this entry was removed from the database
//         response.success(res, `Delete user with id ${userID}`);
//       } else {
//         response.fail(res, `User ${userID} not found`);
//       }
//     })
//     .catch((e) => {
//       console.log(e);
//       res.send(500);
//     });
// };

// const handleEdit = (req, res) => {
//   const { userID, username, password } = req.body;
//   userGame
//     .update({ username, password }, { where: { id: userID } })
//     .then(([result]) => {
//       if (result)
//         return response.success(res, `Edited user with id ${userID} success`);
//       else return response.fail(res, `No user with id ${userID}`);
//     })
//     .catch((err) => response.serverError(res, "Something went wrong" + err));
// };

app.get("/", (req, res) => {
  userGame
    .findAll({
      raw: true,
      order: [["id", "ASC"]],
      include: {
        model: userBiodata,
      },
    })
    .then((users) => {
      console.log(users);
      res.render("index", { users });
    });
});

app.get("/dashboard", (req, res) => {
  userGame
    .findAll({
      raw: true,
      order: [["id", "ASC"]],
    })
    .then((users) => {
      res.render("dashboard", { users });
    });
});

app.get("/user", (req, res) => {
  userGame
    .findAll({
      raw: true,
      order: [["id", "ASC"]],
    })
    .then((users) => {
      res.render("user", { users });
    });
});

app.get("/userbiodata", (req, res) => {
  userGame
    .findAll({
      raw: true,
      order: [["id", "ASC"]],
      include: {
        model: userBiodata,
      },
    })
    .then((users) => {
      res.render("userbiodata", { users });
    });
});

app.get("/userhistory", (req, res) => {
  userGame
    .findAll({
      raw: true,
      order: [["id", "ASC"]],
      include: {
        model: userHistory,
      },
    })
    .then((users) => {
      res.render("userhistory", { users });
    });
});

app.get("/user/:userID", (req, res) => {
  const { userID } = req.params;
  userGame
    .findOne({
      where: {
        id: userID,
      },
      raw: true,
    })
    .then((user) => {
      if (user) {
        res.render("edit", {
          user,
        });
      } else {
        res.render("error", {
          message: `User ${userID} Not Found`,
        });
      }
    })
    .catch((e) => {
      res.send(500);
    });
});

app.get("/api/user", (req, res) => {
  userGame
    .findAll({
      raw: true,
    })
    .then((users) => {
      if (users) {
        response.success(res, users);
      } else {
        response.fail(res, `No data`);
      }
    });
});

// CARA PERTAMA SYNC with .then and catch
app.get("/api/user/:userID", (req, res) => {
  const { userID } = req.params;
  // with .then and .catch
  userGame
    .findOne({
      where: {
        id: userID,
      },
      raw: true,
    })
    .then((user) => {
      if (user) {
        response.success(res, user);
      } else {
        response.fail(res, `User ${userID} not found`);
      }
    })
    .catch((e) => {
      res.send(500);
    });
});

// // CARA KEDUA asynchronus with async-await
// app.get("/api/user/:userID", async (req, res) => {
//   const { userID } = req.params;
//   // with async-await
//   try {
//     let _user = await userGame.findOne({
//       where: {
//         id: userID,
//       },
//       raw: true,
//     });
//     if (_user) {
//       response.success(res, user);
//     } else {
//       response.fail(res, `User ${userID} not found`);
//     }
//   } catch (e) {
//     res.send(500);
//   }
// });

app.post("/api/user/create", handleCreate);
app.post("/api/user", handleCreate);

app.post("/api/user/delete", handleDelete);
app.delete("/api/user", handleDelete);

app.patch("/api/user", handleEdit);

app.listen(4000);

console.log("Server is running at port 4000");
