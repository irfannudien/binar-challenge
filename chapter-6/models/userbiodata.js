"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class userBiodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      userBiodata.belongsTo(models.userGame, {
        foreignKey: "user_id",
      });
    }
  }
  userBiodata.init(
    {
      fullname: DataTypes.STRING,
      address: DataTypes.STRING,
      user_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "userBiodata",
    }
  );

  return userBiodata;
};
