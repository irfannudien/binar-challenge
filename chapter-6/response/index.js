module.exports = {
  success: (response, message) => {
    response.json({
      status: "success",
      message,
    });
    // response.status(200).json({
    //   status: 'success',
    //   message,
    // });
  },
  fail: (response, message) => {
    response.json({
      status: "error",
      message,
    });
  },
};
