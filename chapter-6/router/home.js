const express = require("express");
const { success } = require("../response");

const router = express.Router();

router.get("/", (req, res) => {
  success(res, "Index");
});

module.exports = router;
