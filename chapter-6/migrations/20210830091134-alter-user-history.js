"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.addColumn(
        "userHistories",
        "user_id",
        Sequelize.INTEGER
      );
      await queryInterface.addConstraint("userHistories", {
        type: "foreign key",
        fields: ["user_id"],
        name: "usergame_userhistories_id_fkey",
        references: {
          table: "userGames",
          field: "id",
        },
        onDelete: "CASCADE",
        transaction,
      });
      return transaction.commit();
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     *
     */
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.removeConstraint(
        "userHistory",
        "usergame_userhistories_username_fkey",
        {
          transaction,
        }
      );
      await queryInterface.removeColumn("userHistories", "user_id");
      return transaction.commit();
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  },
};
