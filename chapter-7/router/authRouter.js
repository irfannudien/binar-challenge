const router = require("express").Router();

// Controllers
const auth = require("../controllers/authController");
const { restrictAdmin, restrictPlayer } = require("../middlewares/restrict");

router.get("/dashboard", restrictAdmin, (req, res) => res.render("history"));

// router.get("/dashboard", restrictAdmin, (req, res) =>
//   res.render("history", { Users: "[userList]" })
// );

router.use(restrictPlayer);
router.get("/api/auth/whoami", auth.whoami);
router.post("/api/auth/create-room", auth.create_room);
router.post("/api/auth/gameplay/:room_id", auth.gameplay);

module.exports = router;
