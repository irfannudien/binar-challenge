const { User, room_name, History, sequelize } = require("../models");

let playerOne = "";
let playerOneInput = "";
let gameRound = 1;
let resultArray = [];

// PILIH SUIT
function suit(player1, player2) {
  if (player1 === "gunting") {
    if (player2 === player1) {
      return "draw";
    } else if (player2 === "kertas") {
      return "player 1";
    } else if (player2 === "batu") {
      return "player 2";
    }
  } else if (player1 === "batu") {
    if (player2 === player1) {
      return "draw";
    } else if (player2 === "kertas") {
      return "player 2";
    } else if (player2 === "gunting") {
      return "player 1";
    }
  } else if (player1 === "kertas") {
    if (player2 === player1) {
      return "draw";
    } else if (player2 === "gunting") {
      return "player 2";
    } else if (player2 === "batu") {
      return "player 1";
    }
  }
}

const parse = (data) => {
  const res = {};
  data.forEach((item) => {
    Object.keys(item).map((key) => {
      if (!res[key]) {
        res[key] = item[key];
      } else {
        res[key] += item[key];
      }
    });
  });
  return res;
};

module.exports = {
  // ========== REGISTER ==========
  register: (req, res, next) => {
    // Kita panggil static method register yang sudah kita buat tadi
    User.register(req.body)
      .then((cek) => {
        if (cek) {
          res.redirect("/api/login");
        } else {
          res.send({ message: "something wrong" });
        }
      })
      .catch((err) => next(err));
  },

  // ========== LOGIN ==========
  login: (req, res) => {
    User.authenticate(req.body)
      .then((user) => {
        const { id, username } = user;
        const data = {
          id,
          username,
          tokenLog: user.generateToken(),
        };
        res.send(data);
      })
      .catch((err) => {
        res.send({ message: err.message });
      });
  },

  // ========== LOGIN ADMIN ==========
  loginAdmin: (req, res) => {
    User.authenticate(req.body)
      .then((user) => {
        const { id, username } = user;
        const data = {
          id,
          username,
          tokenLog: user.generateToken(),
        };
        res.redirect("/dashboard");
      })
      .catch((err) => {
        res.send({ message: err.message });
      });
  },

  // ========== PROFILE USER ==========
  whoami: async (req, res) => {
    const currentUser = req.user;
    const { id, username } = currentUser;
    const history = await History.getHistory(id);
    res.send({ id, username, history });
  },

  // ========== CREATE ROOM PLAY ==========
  create_room: (req, res) => {
    room_name
      .generate(req.body.name)
      .then((_room) => {
        res.send(_room);
      })
      .catch((err) => {
        res.send({ message: `${err.message}. Maybe name is duplicate.` });
      });
  },

  // ========== GAMEPLAY & HISTORY ==========
  gameplay: (req, res) => {
    const currentPlayer = req.user;
    //Check p1 available or not
    if (playerOne) {
      //if someone already choose, waiting for other player to choose
      if (currentPlayer.id === playerOne.id && playerOneInput) {
        res.send("You already choose, wait for other players to choose");
      } else {
        // Result gameplay ======
        const playerTwoInput = req.body.option;
        let suitResult = suit(playerOneInput, playerTwoInput);
        if (suitResult === "draw") {
          resultArray.push({ [playerOne.id]: 0, [currentPlayer.id]: 0 });
        } else if (suitResult === "player 1") {
          resultArray.push({ [playerOne.id]: 1, [currentPlayer.id]: 0 });
        } else if (suitResult === "player 2") {
          resultArray.push({ [playerOne.id]: 0, [currentPlayer.id]: 1 });
        }

        playerOne = "";
        playerOneInput = "";
        if (gameRound === 3) {
          gameRound = 1;
          const dataHistory = parse(resultArray);
          const { room_id } = req.params;
          let keys = Object.keys(dataHistory);
          // ======= SAVE DATABASE ========
          sequelize
            .transaction((historyGroup) => {
              return History.create(
                {
                  playerID: parseInt(keys[0]),
                  roomID: parseInt(room_id),
                  result: dataHistory[keys[0]],
                },
                { transaction: historyGroup }
              ).then(() => {
                return History.create(
                  {
                    playerID: parseInt(keys[1]),
                    roomID: parseInt(room_id),
                    result: dataHistory[keys[1]],
                  },
                  { transaction: historyGroup }
                );
              });
            })
            .then(() => {
              const temp = resultArray;
              resultArray = [];
              temp.push({
                winnerID:
                  dataHistory[keys[1]] > dataHistory[keys[0]]
                    ? parseInt(keys[1])
                    : parseInt(keys[0]),
              });
              res.send(temp);
            })
            .catch((err) => {
              console.log("transaction error", err);
              res.send(err);
            });
        } else {
          gameRound++;
          res.send(resultArray);
        }
      }
    }
    // if not available, change id player, save input player id
    else {
      playerOne = currentPlayer;
      playerOneInput = req.body.option;
      res.send("Waiting for other player choose");
    }
  },
};
