const { urlencoded } = require("express");
const express = require("express");
const app = express();
const router = require("./router/router");
const { passportAdmin, passportPlayer } = require("./lib/passport");
const authRouter = require("./router/authRouter");
// const { Users, History } = require("./models");

const { PORT = 4000 } = process.env;

app.use(passportAdmin.initialize());
app.use(passportPlayer.initialize());

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

const db = require("./models/index");
const NEVER_CHANGE = { force: false };
db.sequelize.sync(NEVER_CHANGE); // if true: overwrite the data table every NPM RUN START, false: not overwrite!!

app.set("view engine", "ejs");

// app.get("/dasboard", (req, res) => {
//   Users.findAll({
//     raw: true,
//     order: [["id", "ASC"]],
//     include: {
//       model: History,
//     },
//   }).then((users) => {
//     console.log(users);
//     res.render("history", { Users });
//   });
// });

app.use(router);
app.use(authRouter);
app.listen(PORT, () => {
  console.log(`Server nyala di port ${PORT}`);
});
