"use strict";
const { Model } = require("sequelize");

const { hashSync, compareSync } = require("bcrypt");
const jwt = require("jsonwebtoken");
require("dotenv").config();

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
    // REGISTER ==========
    static #encrypt = (password) => hashSync(password, 10);
    static register = ({ username, role, password }) => {
      const encryptedPassword = this.#encrypt(password);
      return this.create({ username, role, password: encryptedPassword });
    };
    // LOGIN =========
    checkPassword = (password) => compareSync(password, this.password);
    //Method generate JWT
    generateToken = () => {
      const payload = {
        id: this.id,
        username: this.username,
        role: this.role,
      };
      const secret = process.env.JWT_SECRET;
      const token = jwt.sign(payload, secret);
      return token;
    };
    static authenticate = async ({ username, password }) => {
      try {
        const user = await this.findOne({ where: { username } });
        if (!user) return Promise.reject("User not found");

        const isPasswordValid = user.checkPassword(password);
        if (!isPasswordValid) return Promise.reject("Wrong password");

        return Promise.resolve(user);
      } catch (err) {
        return Promise.reject(err);
      }
    };
  }

  User.init(
    {
      username: DataTypes.STRING,
      password: DataTypes.STRING,
      role: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "User",
    }
  );
  return User;
};
