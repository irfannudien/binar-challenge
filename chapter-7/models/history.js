"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class History extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      History.belongsTo(models.room_name, { foreignKey: "roomID" });
    }

    // ===== STATIC hISTORY =====
    static getHistory = (id) => {
      return History.findAll({
        where: {
          playerID: id,
        },
        attributes: ["id", "roomID", "result", "createdAt"],
      });
    };
  }
  History.init(
    {
      playerID: DataTypes.INTEGER,
      roomID: DataTypes.INTEGER,
      result: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "History",
    }
  );
  return History;
};
