"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class room_name extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // room_name.hasMany(models.History, { foreignKey: "room_id" });
    }

    static generate = (name) => {
      return room_name.create({ name });
    };
  }
  room_name.init(
    {
      name: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "room_name",
    }
  );
  return room_name;
};
